</html>
<!doctype html>
<html lang="fr">
  <head>
    <!-- meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Site qui présente mes projets et ma personne">
    <meta name="keywords" content="Océanographie ma life, Bretagne, Norvège, poulpes ">
    <meta name="author" content="Fabien Couarzen">

    <!--Liens CSS et FontAwesome -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="contact.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    
    <!-- Fav Icon -->
    <link rel="shortcut icon" href="media/favicon.ico" type="image/x-icon">
    <link rel="icon" href="media/favicon.ico" type="image/x-icon">
    <!-- Titre -->
    <title>Fabien Couarzen : Contact</title>
  </head>
  
  <body>
      <!-- Barre de navigation -->
    <div class="container-fluid  sticky-top">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark px-5">
            <!-- Brand name -->
            <a class="navbar-brand d-flex" href="index.html">
                <img src="media/bear_mini.jpg" width="60" height="60" class="photo-bear d-inline-block align-top" alt="Photo de Fabien Couarzen">
                <p class="align-self-center m-0 px-4">Fabien Couarzen</p>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item px-3">
                    <a class="nav-link" href="#">Océanographie</a>
                    </li>
                    <li class="nav-item active px-3">
                    <a class="nav-link" href="contact.php">Contact</a>
                    </li>
                </ul>
               <!-- Barre de recherche  -->
            <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form>
            </div>
      </nav>
    </div>
    <!-- Image de header et citation -->
    <header class="container-fluid p-0 d-flex justify-content-center align-items-center">
        <img class="img-fluid" src="media/header_landscape.jpg" alt="paysage naturel norvégien" width="2000" height="833">
        <!-- <span class="position-relative">Test</span> -->
    </header>
    <!-- Partie info de contact -->
    <section class="container-fluid" id="contact_info">
        <article class="container py-3">
            <div class="row justify-content-center">
                    <h2 class="text-center pb-3">Contactez Moi</h2>
            </div>
            <hr>
            <div class="row">
                <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 d-flex flex-column flex-lg-row flex-xl-row justify-content-around justify-content-center align-items-center">
                    <div class="icon_frame d-flex justify-content-center align-items-center">
                        <a class="icon text-center" href=""><i class="fas fa-map-marked-alt"></i></a>
                    </div>  
                    <p class="text_contact">Flekkefjord Museum<br>
                    Dr. Krafts gate 15 4400 Flekkefjord<br>
                    Norway</p>
                </div>
                <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 d-flex flex-column flex-lg-row flex-xl-row justify-content-around justify-content-md-start align-items-center">
                    <div class="icon_frame d-flex justify-content-center align-items-center">
                        <a class="icon text-center" href=""><i class="fas fa-mobile-alt"></i></a>
                    </div>  
                    <a class="text_contact text-center" href="callto:#">+471 23 45 67 89</a>
                </div>
                <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 d-flex flex-column flex-lg-row flex-xl-row justify-content-around justify-content-md-start align-items-center">
                    <div class="icon_frame d-flex justify-content-center align-items-center">
                        <a class="icon text-center" href=""><i class="fas fa-envelope-open"></i></a>
                    </div>  
                    <a class="text_contact text-center" href="mailto:#">fabien.couarzen@gmail.corn</a>
                </div>
            </div>
        </article>
    </section>
<!-- partie carte interactive -->
    <section class="container-fluid" id="map">
        <article class="p-0 m-0">
            <iframe class="embed-responsive" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5930.2346653579625!2d6.65862665554583!3d58.298005726985835!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4639ddda582513bf%3A0xa7368fd160556619!2sFlekkefjord+Museum!5e0!3m2!1sfr!2sfr!4v1538742737362" width="1200" height="500" frameborder="0" style="border:0"></iframe>
        </article>
    </section>
    <!-- partie formulaire de contact -->
    <section class="container-fluid" id="formulaire">
        <article class="container py-3">
            <form class="pt-5" action="traitement.php" method="post">
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-5 col-xl-5">
                        <label for=""><h2 class="title_form">Ecrivez moi</h2></label>
                        <div class="d-flex justify-content-between w-100">
                            <label class="boite_radio" for="mr">
                                <input type="radio" id="mr" name="gender" value="mr" checked>
                                <span class="radiobtn"></span>
                                M.
                            </label>
                            <label class="boite_radio" for="mme">
                                <input type="radio" id="mme" name="gender" value="mme">
                                <span class="radiobtn"></span>
                                Mme
                            </label>
                            <label class="boite_radio" for="mlle">
                                <input type="radio" id="mlle" name="gender" value="mlle">
                                <span class="radiobtn"></span>
                                Mlle
                            </label>
                        </div>
                        <p>
                            <label class="grey_form" for="nom">Nom</label><br>
                            <input class="saisie w-100" type="text" id="nom" name="nom" placeholder="Jean Dupont">
                        </p>
                        <p>
                            <label class="grey_form" for="email">E-mail</label><br>
                            <input  class="saisie w-100" type="email" id="email" name="email" placeholder="jean.dupont@gougoule.com">
                        </p>
                        <p>
                            <label class="grey_form" for="pays">Pays</label><br>
                            <select class="saisie w-100" name="pays" id="pays">
                                <option class="pays">France</option>
                                <option class="pays">Norvège</option>
                                <option class="pays">Suisse</option>
                                <option class="pays">Zimbabwe</option>
                                <option class="pays">Croatie</option>
                            </select>
                        </p>
                        <p>
                            <label class="grey_form" >Vérification</label><br>
                            <label class="boite_check" for="robot">Je ne suis pas un robot
                                <input type="checkbox" name="robot" id="robot" value="nobot">
                                <span class="checkmark"></span>
                            </label>
                        </p>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-7 col-xl-7 d-flex flex-column">
                        <label class="grey_form" for="message">Message</label>
                        <textarea class="w-100 h-100" placeholder="Ecrivez-moi ici." name="message" placeholder="Ecrivez-moi ici !" id="message">

                        </textarea>
                        <input class="w-100 btnEnvoi" id="btnEnvoi" type="submit" value="Envoyer">
                    </div>
                </div>
            </form>
        </article>
    </section>

   <footer class="container-fluid">
      <div class="container">
          <div class="d-flex flex-column flex-md-row flex-lg-row flex-xl-row justify-content-around align-items-center py-2">   
              <a class="menu_footer py-4" href="index.html">Fabien Couarzen</a>
              <a class="menu_footer py-4" href="#">Recherches</a>
              <a class="menu_footer py-4" href="contact.php">Contact</a>
          </div>
          <div class="d-flex justify-content-around align-items-center py-2">   
              <a href="#"><i class="fab fa-facebook-square"></i></a>
              <a href="#"><i class="fab fa-twitter-square"></i></a>
              <a href="#"><i class="fab fa-instagram"></i></a>
              <a href="#"><i class="fab fa-linkedin"></i></a>
          </div>
          <hr>  
          <div>
              <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Et incidunt placeat atque aut vero voluptates, ea culpa quia consectetur molestiae dolore eum ut nostrum, natus totam aliquam deleniti assumenda accusantium!<span class="copyright">2018 ©</span></p>
              
          </div>
      </div>
  </footer>
   
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    
    <!-- Optional JavaScript -->
    <!-- <script src="app.js"></script> -->
</body>
</html>