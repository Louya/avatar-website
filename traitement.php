<html>

<head>
 <!-- meta tags -->
 <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Site qui présente mes projets et ma personne">
    <meta name="keywords" content="Océanographie ma life, Bretagne, Norvège, poulpes ">
    <meta name="author" content="Fabien Couarzen">

    <!--Liens CSS et FontAwesome -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="contact.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    
    <!-- Fav Icon -->
    <link rel="shortcut icon" href="media/favicon.ico" type="image/x-icon">
    <link rel="icon" href="media/favicon.ico" type="image/x-icon">
    <!-- Titre -->
    <title>Ma page de traitement</title>
</head>

<body>
<div class="container-fluid  sticky-top">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark px-5">
            <!-- Brand name -->
            <a class="navbar-brand d-flex" href="index.html">
                <img src="media/bear_mini.jpg" width="60" height="60" class="photo-bear d-inline-block align-top" alt="Photo de Fabien Couarzen">
                <p class="align-self-center m-0 px-4">Fabien Couarzen</p>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item px-3">
                    <a class="nav-link" href="#">Océanographie</a>
                    </li>
                    <li class="nav-item active px-3">
                    <a class="nav-link" href="contact.html">Contact</a>
                    </li>
                </ul>
               <!-- Barre de recherche  -->
            <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form>
            </div>
      </nav>
    </div>
    <header class="container-fluid">
        <div class="container  d-flex justify-content-center py-3">
            <h1>Merci de m'avoir ecrit !</h1>
        </div>
    </header>
    <main class="container-fluid">
        <section class="container d-flex flex-column justify-content-center py-4">
        <?php    
    //fonction de debbug et d'affichage-----------------------------------------------
    function console_log( $data ){
        echo '<script>';
        echo 'console.log('. json_encode( $data ) .')';
        echo '</script>';
      }
    // récupération des données du formulaire ---------------------------------------
        $gender = $_POST['gender'];
        $nom = $_POST['nom'];
        $email = $_POST['email'];
        $pays = $_POST['pays'];
        $robot = $_POST['robot'];
        $message = $_POST['message'];
        $erreur = array();
    // Vérification du formulaire ---------------------------------------------------
        // vérification de champs vides
        if ('' == $nom) {
            $blanckNom = 'Veuillez rentrer votre nom.<br>';
            array_push($erreur, $blanckNom);
        }
        if ('' == $email) {
            $blanckEmail = 'Veuillez rentrer votre mail.<br>';
            array_push($erreur, $blanckEmail);
        } else {
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $formatEmail = "Le format de votre adresse mail n'est pas valide.";
                array_push($erreur, $formatEmail);
            }
        }
        // if (isset() &&  == $robot) {
        //     $blanckRobot = 'Veuillez rentrer votre cocher la case de vérification.<br>';
        //     array_push($erreur, $blancRobot);
        // }
        // vérification de formats de données
        if (!preg_match("/^[a-zA-Z ]*$/",$nom)) {
            $formatNom = "Veuillez utiliser des lettres uniquement pour le nom.<br>";
            array_push($erreur, $formatNom);
          }
        
        // affichage du message d'erreur
        console_log($erreur);
        if (count($erreur) != 0){
            echo "<p class='erreur'>Oups, il a y a un soucis<br></p>";
            echo $blanckNom.'<br>';
            echo $blanckEmail.'<br>';
            echo $blanckRobot.'<br>';
            echo $formatNom.'<br>';
            echo $formatEmail.'<br>';
            echo '<a href="contact.php" class="btn btn-primary my-2">Retour au formulaire</a>';

        } else {
            // affichage des resultats
            echo 'Ton genre : '.$gender."<br>";
            echo 'Ton nom : '.$nom."<br>";
            echo 'Ton mail : '.$email."<br>";
            echo 'Ton pays : '.$pays."<br>";
            echo 'Es-tu un robot ? : '.$robot."<br>";
            echo 'Tu a écrit ça : '.$message."<br>";
            mailAuto();
        }
        
    
        // Envoi du mail--------------------------------------------------------------
        function mailAuto(){
            // Déclaration de l'adresse de destination.
            $Listmail = array('fabien.c@codeur.online'); 
            $mail;
            $nbMails = count($Listmail);
            // boucle d'envoi des mails
            for ($i = 0; $i < $nbMails; $i++){
                $mail = $Listmail[$i];
                sendmail($mail, $gender, $nom, $email, $pays, $robot, $message);
            }
        }
            
        function sendmail($data, $gender, $nom, $email, $pays, $robot, $message){
            // On filtre les serveurs buggés
            if (!preg_match("#^[a-z0-9._-]+@(hotmail|live|msn).[a-z]{2,4}$#", $mail)){
                $passage_ligne = "\r\n";
            }
            else{
            $passage_ligne = "\n";
            }   
            //=====Déclaration des messages au format texte et au format HTML.
            $message_txt = "Vous avez reçu un formulaire !
                            De la part de {$gender} {$nom},
                            son email : {$email} ,
                            Venant du pays suivant : {$pays},
                            Avec le message suivant : {$message}";
            $message_html = "<html>
                                <head>
                                </head>
                                <body>
                                    <h1>Vous avez reçu un formulaire !</h1>
                                    <p>
                                    De la part de <b>{$gender} {$nom}</b>,
                                    <br>son email : <b>{$email}</b>,
                                    <br>Venant du pays suivant : <b>{$pays}</b>,
                                    <br>Avec le message suivant : {$message}
                                    </p>
                                </body>
                            </html>";
            //==========
            //=====Création de la boundary
            $boundary = "-----=".md5(rand());
            //==========
            //=====Définition du sujet.
            $sujet = "/!\ {$nom} vous a envoyé un message depuis votre site avatar ! :D ";
            //=========
            //=====Création du header de l'e-mail.
            $header = "From: \"Site Avatar\"<site.avatar@trash.no>".$passage_ligne;
            $header.= "Reply-to: \"Fabien Cazalet\"<fabien.c@codeur.online>".$passage_ligne;
            $header.= "MIME-Version: 1.0".$passage_ligne;
            $header.= "Content-Type: multipart/alternative;".$passage_ligne." boundary=\"$boundary\"".$passage_ligne;
            //==========
            //=====Création du message. 
            $message = $passage_ligne."--".$boundary.$passage_ligne;
            //=====Ajout du message au format texte.
            $message.= "Content-Type: text/plain; charset=\"ISO-8859-1\"".$passage_ligne;
            $message.= "Content-Transfer-Encoding: 8bit".$passage_ligne;
            $message.= $passage_ligne.$message_txt.$passage_ligne;
            //==========
            $message.= $passage_ligne."--".$boundary.$passage_ligne;
            //=====Ajout du message au format HTML
            $message.= "Content-Type: text/html; charset=\"ISO-8859-1\"".$passage_ligne;
            $message.= "Content-Transfer-Encoding: 8bit".$passage_ligne;
            $message.= $passage_ligne.$message_html.$passage_ligne;
            //==========
            $message.= $passage_ligne."--".$boundary."--".$passage_ligne;
            $message.= $passage_ligne."--".$boundary."--".$passage_ligne;
            //==========
            //=====Envoi de l'e-mail.
            mail($data,$sujet,$message,$header);
        }
        //Fin du mail --------------------------------------------------------------
    ?>
        </section>
    </main>
    <footer class="container-fluid">
      <div class="container">
          <div class="d-flex flex-column flex-md-row flex-lg-row flex-xl-row justify-content-around align-items-center py-2">   
              <a class="menu_footer py-4" href="index.html">Fabien Couarzen</a>
              <a class="menu_footer py-4" href="#">Recherches</a>
              <a class="menu_footer py-4" href="contact.php">Contact</a>
          </div>
          <div class="d-flex justify-content-around align-items-center py-2">   
              <a href="#"><i class="fab fa-facebook-square"></i></a>
              <a href="#"><i class="fab fa-twitter-square"></i></a>
              <a href="#"><i class="fab fa-instagram"></i></a>
              <a href="#"><i class="fab fa-linkedin"></i></a>
          </div>
          <hr>  
          <div>
              <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Et incidunt placeat atque aut vero voluptates, ea culpa quia consectetur molestiae dolore eum ut nostrum, natus totam aliquam deleniti assumenda accusantium!<span class="copyright">2018 ©</span></p>
              
          </div>
      </div>
  </footer>

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>

</html>